<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

<p>INSTALLATION</p>

## Clone Project

Run Command : 
git clone https://gitlab.com/izzulSyahmi/laravel-training.git

## Redirect to Project

using Gitbash or any other terminal

Run Command : 
cd companies-management

## Install Package

composer install

## copy env file / Create Key

Run Command : 
cp .env.example .env

Run Command : 
php artisan key:generate

## env file

if using Xampp

APP_URL=http://localhost/companies-management/public

if using php artisan serve

APP_URL=http://127.0.0.1:8000

p/s it is for livewire path

## migrate database and seed

run command
php artisan migrate

run command
php artisan db:seed

## run project

1) Through terminal

run command : 
php artisan serve

redirect to http://127.0.0.1:8000

2) Xampp

redirect to http://localhost/companies-management/public

## Login Detail

Email : admin@admin.com

Password : password
