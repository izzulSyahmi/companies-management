<?php

namespace App\Http\Livewire;

use Auth;
use Livewire\Component;
use Livewire\WithPagination;

use App\Models\Companies;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File; 

class CompaniesList extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $isRegister = false;

    public $selectedCompany = [];
    public $oldImage;
    public $reviewImage;
    public $routeImage;

    public $tableLength = 10;

    public $search = [

    ];

    protected $rules = [
        'selectedCompany.name' => 'required',
        'selectedCompany.email' => ["required","email"],
        'selectedCompany.website' => ['required','regex:/^[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b(?:[-a-zA-Z0-9()@:%_\\+.~#?&\\/=]*)$/']
    ];

    public function render()
    {
        $dt_companies = new Companies();

        if(!empty($this->search['name']))
        {
            $dt_companies = $dt_companies->where('name','LIKE', "%".$this->search['name']."%");
        }
        if(!empty($this->search['email']))
        {
            $dt_companies = $dt_companies->where('email','LIKE', "%".$this->search['email']."%");
        }

        if(!empty($this->search['website']))
        {
            $dt_companies = $dt_companies->where('website','LIKE', "%".$this->search['website']."%");
        }


        return view('livewire.companies-list',[
            "dt_companies" => $dt_companies->paginate($this->tableLength)
        ]);
    }

    public function viewCompany($type , $idCompany = null)
    {
        $this->reviewImage = "";
        $this->selectedCompany = [];
        $this->routeImage = 'temp-image';

        if($type == 1)
        {
            $this->isRegister = true;
        }
        else if($type == 2)
        {
            $this->isRegister = false;
            $this->selectedCompany = Companies::where('id',$idCompany)->first()->toArray();

            $this->reviewImage = $this->selectedCompany['logo'];
            $this->oldImage = $this->selectedCompany['logo'];
            
            $this->routeImage = 'get-image';
        }
        
        $this->emit('showModal', 'modalCompanyView');
    }

    public function clearSearch()
    {
        $this->search = [];
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatedTableLength()
    {
        $this->resetPage();
    }

    public function updatedSelectedCompany($value , $key)
    {
        if($key == "logo")
        {
            $this->reviewImage = $this->selectedCompany['logo']->getFilename();
            $this->routeImage = 'temp-image';
        }
    }

    public function registerCompany()
    {
        try {

            $this->validate();
            
            $this->validate([
                'selectedCompany.logo' =>  ['required','image']
            ]);

            $this->selectedCompany['logo']->storeAs('public',$this->reviewImage);
            $this->selectedCompany['logo'] = $this->reviewImage;

            //delete temp img
            Storage::delete('livewire-tmp/'.$this->reviewImage);
            
            DB::beginTransaction();
            Companies::insert([
                $this->selectedCompany
            ]);
            DB::commit();

            $this->emit('hideModal', 'modalCompanyView');

            $this->emit('swal:alert', [
                'position' => 'bottom-end',
                'backdrop' => false,
                'timer' => 2000,
                'icon' => 'success',
                'title' => '<b style="font-size:25px;font-weight-boldest">Success Register ['.$this->selectedCompany['name'].'] Company</b>',
                'html' => ''
            ]);

        } catch (Exception $e) {
            DB::rollBack();
        } 
    }

    public function updateCompany()
    {
        try {

            $this->validate();

            if($this->reviewImage != $this->selectedCompany['logo'])
            {
                $this->validate([
                    'selectedCompany.logo' =>  ['required','image']
                ]);

                // delete old image
                Storage::delete('public/'.$this->oldImage);

                $this->selectedCompany['logo']->storeAs('public',$this->reviewImage);

                // delete temp image
                Storage::delete('livewire-tmp/'.$this->reviewImage);
            }
            $this->selectedCompany['logo'] = $this->reviewImage;
            
            DB::beginTransaction();
            Companies::where('id',$this->selectedCompany['id'])
            ->update([
                'name' => $this->selectedCompany['name'],
                'email' => $this->selectedCompany['email'],
                'logo' => $this->selectedCompany['logo'],
                'website' => $this->selectedCompany['website'],
            ]);
            DB::commit();

            $this->emit('hideModal', 'modalCompanyView');

            $this->emit('swal:alert', [
                'position' => 'bottom-end',
                'backdrop' => false,
                'icon' => 'success',
                'timer' => 2000,
                'title' => '<b style="font-size:25px;font-weight-boldest">Success Update ['.$this->selectedCompany['name'].'] Company</b>',
                'html' => ''
            ]);

        } catch (Exception $e) {
            DB::rollBack();
        } 
    }

    public function deleteCompany($id , $filename , $name)
    {
        try {

            DB::beginTransaction();

            Storage::delete('public/'.$filename);

            Companies::findOrFail($id)->delete();

            DB::commit();

            $this->emit('swal:alert', [
                'position' => 'bottom-end',
                'backdrop' => false,
                'icon' => 'success',
                'timer' => 2000,
                'title' => '<b style="font-size:25px;font-weight-boldest">Success Delete ['.$name.'] Company</b>',
                'html' => ''
            ]);

        } catch (Exception $e) {
            DB::rollBack();
        } 
    }

    public function logout()
    {
        Auth::logout();
        
        return redirect(route('login'));
    }
}
