<?php

namespace App\Http\Livewire;

use Auth;
use App\Models\User;
use Livewire\Component;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class Login extends Component
{
    public $email = "";
    public $password = "";

    protected $rules = [
        "email" => ["required","email"],
        "password" => 'required'
    ];

    public function mount()
    {
        if (Auth::user()) {
            return redirect(route('company-list'));
        }
    }

    public function render()
    {
        return view('livewire.login');
    }

    public function clickLogin()
    {
        $this->validate();

        try {
            $userData = User::where("email",$this->email)->first();

            if(empty($userData))
            {
                return $this->emit('swal:alert', [
                    'icon' => 'error',
                    'title' => '<b style="font-size:25px;font-weight-boldest">Account Not Found</b>',
                    'html' => ''
                ]);
            }
            
            DB::beginTransaction();

            if(Hash::check($this->password , $userData->password))
            {
                // Auth::attempt(array('email' => trim($this->email), 'password' => $this->password));
                Auth::login($userData);
                
                return redirect(route('company-list'));
            }
            else
            {
                return $this->emit('swal:alert', [
                    'icon' => 'error',
                    'title' => '<b style="font-size:25px;font-weight-boldest">Wrong Password</b>',
                    'html' => ''
                ]);
            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            
        } catch (\Illuminate\Validation\ValidationException $e) {
            DB::rollBack();
        }

        // dd(Hash::check($this->password, User::where("email",$this->email)->pluck('password')->first()));
        // dd("TEST");
    }
}
