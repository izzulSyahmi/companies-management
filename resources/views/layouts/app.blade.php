<!DOCTYPE html>
<html lang="en">

{{--begin::Head--}}

<head>
    <base href="">
	<meta charset="utf-8" />
	<title>Companies Management</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<meta name="csrf-token" content="{{ csrf_token() }}">

	@livewireStyles
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>

    {{-- jquery --}}
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</head>

<body>
        
    <div class="container-fluid">
        {{ $slot }}
    </div>
    
    @livewireScripts

</body>
<style>
  /* Importing fonts from Google */
  /* @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&display=swap'); */

  /* Reseting */
  * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
      font-family: 'Poppins', sans-serif;
  }

  body {
      background: #ecf0f3;
  }

  .wrapper {
      max-width: 350px;
      min-height: 500px;
      margin: 80px auto;
      padding: 40px 30px 30px 30px;
      background-color: #ecf0f3;
      border-radius: 15px;
      box-shadow: 13px 13px 20px #cbced1, -13px -13px 20px #fff;
  }

  .logo {
      width: 80px;
      margin: auto;
  }

  .logo img {
      width: 100%;
      height: 80px;
      object-fit: cover;
      border-radius: 50%;
      box-shadow: 0px 0px 3px #5f5f5f,
          0px 0px 0px 5px #ecf0f3,
          8px 8px 15px #a7aaa7,
          -8px -8px 15px #fff;
  }

  .wrapper .name {
      font-weight: 600;
      font-size: 1.4rem;
      letter-spacing: 1.3px;
      padding-left: 10px;
      color: #555;
  }

  .wrapper .form-field input {
      width: 100%;
      display: block;
      border: none;
      outline: none;
      background: none;
      font-size: 1.2rem;
      color: #666;
      padding: 10px 15px 10px 10px;
      /* border: 1px solid red; */
  }

  .wrapper .form-field {
      padding-left: 10px;
      margin-bottom: 20px;
      border-radius: 20px;
      box-shadow: inset 8px 8px 8px #cbced1, inset -8px -8px 8px #fff;
  }

  .wrapper .form-field .fas {
      color: #555;
  }

  .wrapper .btn {
      box-shadow: none;
      width: 100%;
      height: 40px;
      background-color: #03A9F4;
      color: #fff;
      border-radius: 25px;
      box-shadow: 3px 3px 3px #b1b1b1,
          -3px -3px 3px #fff;
      letter-spacing: 1.3px;
  }

  .wrapper .btn:hover {
      background-color: #039BE5;
  }

  .wrapper a {
      text-decoration: none;
      font-size: 0.8rem;
      color: #03A9F4;
  }

  .wrapper a:hover {
      color: #039BE5;
  }

  @media(max-width: 380px) {
      .wrapper {
          margin: 30px 20px;
          padding: 40px 15px 15px 15px;
      }
  }
</style>


<script>
        ////////////////////////////////////////////////
    // livewire show/hide modal 
    window.livewire.on('hideModal', (modalID) => {
        $('#'+modalID).modal('hide');
    });

    window.livewire.on('showModal', (modalID) => {
        $('#'+modalID).modal('show');
    });

    window.livewire.on('swal:alert', (event) => { 
        Swal.fire({
            position: event.position == null ? 'center' : event.position,
            backdrop : event.backdrop == null ? true : event.backdrop,
            icon: event.icon,
            title: event.title,
            timer: event.timer == null ? '10000' : event.timer,
            width: 720,
            html: event.html,
            confirmButtonColor: '#1BC5BD',
            confirmButtonText: 'Close',
            showConfirmButton: event.showButton,
        })
    });
</script>
