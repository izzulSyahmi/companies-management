
<div class="card p-10">
    <div class="container-fluid">
        <div class="card-header border-0 pt-3">
            <h3 class="card-title align-items-start flex-column mt-7">
                <span class="card-label font-weight-bolder text-dark-75">
                    Companies
                </span>
            </h3>
            <div class="card-toolbar pt-3" style="display:block" >
                <button wire:click="logout"
                    class=" float-right btn btn-danger font-weight-bolder font-size-sm">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-fullscreen-exit" viewBox="0 0 16 16">
                        <path d="M5.5 0a.5.5 0 0 1 .5.5v4A1.5 1.5 0 0 1 4.5 6h-4a.5.5 0 0 1 0-1h4a.5.5 0 0 0 .5-.5v-4a.5.5 0 0 1 .5-.5zm5 0a.5.5 0 0 1 .5.5v4a.5.5 0 0 0 .5.5h4a.5.5 0 0 1 0 1h-4A1.5 1.5 0 0 1 10 4.5v-4a.5.5 0 0 1 .5-.5zM0 10.5a.5.5 0 0 1 .5-.5h4A1.5 1.5 0 0 1 6 11.5v4a.5.5 0 0 1-1 0v-4a.5.5 0 0 0-.5-.5h-4a.5.5 0 0 1-.5-.5zm10 1a1.5 1.5 0 0 1 1.5-1.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 0-.5.5v4a.5.5 0 0 1-1 0v-4z"/>
                    </svg>
                    Logout
                </button>
                <button wire:click="viewCompany('1')"
                    class=" float-right btn btn-primary font-weight-bolder font-size-sm">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-plus" viewBox="0 0 16 16">
                        <path d="M8.5 6a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V10a.5.5 0 0 0 1 0V8.5H10a.5.5 0 0 0 0-1H8.5V6z"/>
                        <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z"/>
                    </svg>
                    Register New Company
                </button>
            </div>
        </div>

        @include('livewire.modal.company-view')

        <div class="card-body pt-5 pt-5">
            <div class="form-group row mb-3">
                <label
                    class="col-1 col-form-label offset-9">
                    Table Length
                </label>
                <div class="col-2">
                    <select 
                        class="form-control text-uppercase custom-select"
                        wire:model.lazy="tableLength">
                            <option value=5 >5</option>
                            <option value=10 selected="selected">10</option>
                            <option value=20 >20</option>
                            <option value=50 >50</option>
                            <option value=100 >100</option>
                    </select>
                </div>
            </div>
            <div class="table-responsive ma-5">
                <table class="table table-head-custom table-vertical-center">
                    <thead>
                        <tr class="text-left">
                            <th style="">No.</th>
                            <th width="210px">
                                
                            </th>
                            <th >
                                <input type="text" class="form-control form-control-solid mb-2" placeholder="Name"
                                wire:model="search.name" autocomplete="">
                                Name
                            </th>
                            <th >
                                <input type="text" class="form-control form-control-solid mb-2" placeholder="Email"
                                wire:model="search.email" autocomplete="">
                                Email
                            </th>
                            <th >
                                <input type="text" class="form-control form-control-solid mb-2" placeholder="Website"
                                wire:model="search.website" autocomplete="">
                                Website
                            </th>
                            <th class="pr-0 text-right" width="230px">
                                <button wire:click.prevent="clearSearch"
                                    class="btn btn-warning font-weight-bolder mb-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-repeat" viewBox="0 0 16 16">
                                        <path d="M11.534 7h3.932a.25.25 0 0 1 .192.41l-1.966 2.36a.25.25 0 0 1-.384 0l-1.966-2.36a.25.25 0 0 1 .192-.41zm-11 2h3.932a.25.25 0 0 0 .192-.41L2.692 6.23a.25.25 0 0 0-.384 0L.342 8.59A.25.25 0 0 0 .534 9z"/>
                                        <path fill-rule="evenodd" d="M8 3c-1.552 0-2.94.707-3.857 1.818a.5.5 0 1 1-.771-.636A6.002 6.002 0 0 1 13.917 7H12.9A5.002 5.002 0 0 0 8 3zM3.1 9a5.002 5.002 0 0 0 8.757 2.182.5.5 0 1 1 .771.636A6.002 6.002 0 0 1 2.083 9H3.1z"/>
                                    </svg>
                                    Clear
                                </button><br/>
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($dt_companies as $indexKey => $company )
                        <tr>
                            <td >
                                <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">
                                    {{ $dt_companies->firstItem() + $indexKey }}
                                </span>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">
                                    <img src='{{ route('get-image', $company->logo) }}'
                                        width="150px"
                                        alt=""/>
                                </span>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">
                                    {{ $company->name }}
                                </span>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">
                                    {{ $company->email }}
                                </span>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">
                                    {{ $company->website }}
                                </span>
                            </td>
                            <td>
                                <button wire:click.prevent="viewCompany('2','{{$company->id}}')"
                                    class="btn btn-success font-weight-bolder ">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                                        <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                                    </svg>
                                    Update
                                </button>

                                <button wire:click.prevent="deleteCompany('{{$company->id}}','{{$company->logo}}','{{$company->name}}')"
                                    class="btn btn-danger font-weight-bolder ">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                                        <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z"/>
                                    </svg>
                                      Delete
                                </button>
                            </td>
                        </tr>

                        @empty
                        <tr class="text-center mt-2 mb-2">
                            <td colspan="6" class="" valign="top">
                                <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">
                                    No Data Available
                                </span>
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>

                @if(!empty($dt_companies))
                    {{ $dt_companies->links() }}
                @endif
            </div>
        </div>

    </div>
</div>
