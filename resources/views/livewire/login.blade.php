<div>
    <div class="wrapper">
        <div class="logo">
            <img src="https://w7.pngwing.com/pngs/541/399/png-transparent-computer-icons-business-process-company-enterprise-architecture-company-building-text-rectangle.png" alt="">
        </div>
        <div class="text-center mt-4 name">
            Company Management 
        </div>
        <div class="p-3 mt-3">
            <div class="form-field d-flex align-items-center">
                <span class="far fa-user"></span>
                <input 
                    type="email" wire:model.lazy="email" placeholder="Email">
            </div>               
             @error('email')
                <div class="text-danger" style='font-size:12px;'>
                    <strong>{{ $message }}</strong>
                </div>
            @enderror
            <div class="form-field d-flex align-items-center">
                <span class="fas fa-key"></span>
                <input 
                    type="password" wire:model.lazy="password" placeholder="Password">
            </div>
            @error('password')
                <div class="text-danger" style='font-size:12px;'>
                    <strong>{{ $message }}</strong>
                </div>
            @enderror
            <button wire:click="clickLogin" class="btn mt-3">Login</button>
        </div>
        <div class="text-center fs-6">
            {{-- <a href="#">Forget password?</a> or <a href="#">Sign up</a> --}}
        </div>
    </div>
</div>

{{-- @section('page-script')
<script>
    document.addEventListener('livewire:load', function() {
        Swal.fire(
            'The Internet?',
            'That thing is still around?',
            'question'
            )
        })
</script>
@stop --}}
