<div wire:ignore.self class="modal fade" data-backdrop="static" id="modalCompanyView" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bolder text-dark-75" id="exampleModalLabel">
                    Company
                </h5>
            </div>
            <div class="modal-body">
                <div class="form-group row mb-2">
                    <label class="col-lg-3 col-form-label font-weight-bolder">Name</label>
                    <div class="col-lg">
                        <input type="text" class="form-control form-control-sm form-control-solid @error('selectedCompany.name') is-invalid @enderror"
                            placeholder="" wire:model.lazy="selectedCompany.name">
                        @error('selectedCompany.name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-2">
                    <label class="col-lg-3 col-form-label font-weight-bolder">Email</label>
                    <div class="col-lg">
                        <input type="email" class="form-control form-control-sm form-control-solid @error('selectedCompany.email') is-invalid @enderror"
                            placeholder="" wire:model.lazy="selectedCompany.email">
                        @error('selectedCompany.email')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    </div>

                <div class="form-group row mb-2">
                    <label class="col-lg-3 col-form-label font-weight-bolder">Logo</label>
                    <div class="col-lg">
                        <input type="file" class="form-control form-control-sm form-control-solid @error('selectedCompany.logo') is-invalid @enderror"
                            placeholder="" wire:model="selectedCompany.logo"  accept=".png, .jpg, .jpeg">
                        @error('selectedCompany.logo')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>

                @if(!empty($reviewImage))
                <div class="form-group row mb-2">
                    <label class="col-lg-3 col-form-label font-weight-bolder"></label>
                    <div class="col-lg">
                        <img src='{{ route($routeImage, $reviewImage) }}'
                        width="450px"
                        alt=""/>
                    </div>
                </div>
                @endif
                

                <div class="form-group row mb-2">
                    <label class="col-lg-3 col-form-label font-weight-bolder">Website</label>
                    <div class="col-lg">
                        <input type="text" class="form-control form-control-sm form-control-solid @error('selectedCompany.website') is-invalid @enderror"
                            placeholder="" wire:model.lazy="selectedCompany.website">
                        @error('selectedCompany.website')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="card-footer text-right bg-gray-100 border-top-0">
                    
                    @if($isRegister)
                    <button wire:click.prevent="registerCompany"
                        class="btn btn-success font-weight-bolder font-size-sm">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-plus" viewBox="0 0 16 16">
                            <path d="M8.5 6a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V10a.5.5 0 0 0 1 0V8.5H10a.5.5 0 0 0 0-1H8.5V6z"/>
                            <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z"/>
                        </svg>
                        Register
                    </button>

                    @else
                    <button wire:click.prevent="updateCompany"
                        class="btn btn-success font-weight-bolder font-size-sm">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                            <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                        </svg>
                        Update
                    </button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>