<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Http\Livewire\Login;
use App\Http\Livewire\CompaniesList;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function (Request $request) {

    if (Auth::user()) {
        return Redirect::to('/company-list');
    }

    return Redirect::to('/login');
});

Route::get('/login',
    Login::class
)->name('login');

Route::group(['middleware' => [
    'auth'],
], function () {
    
    Route::get('/company-list',
        CompaniesList::class
    )->name('company-list');

    Route::get('/temp-image/{image}',function($image){
        if(Storage::exists('livewire-tmp/'.$image))
        {
            $content = Storage::get('livewire-tmp/'.$image);
            $mime = Storage::mimeType('livewire-tmp/'.$image);
            $response = Response::make($content, 200);
            $response->header("Content-Type", $mime);
            return $response;
        }
        else
        {
            return "";
        }
    })
    ->name('temp-image');

    Route::get('/get-image/{image}',function($image){
        if(Storage::exists('public/'.$image))
        {
            $content = Storage::get('public/'.$image);
            $mime = Storage::mimeType('public/'.$image);
            $response = Response::make($content, 200);
            $response->header("Content-Type", $mime);
            return $response;
        }
        else
        {
            return "";
        }
    })
    ->name('get-image');
});
